# Istruzioni per la giuria

Alcune informazioni utili per la giuria dell'Hackathon.

# Logistica

## Sabato 07 Settembre 2019

**13:00:** *Dove: Area Hackathon* I partecipanti alla sfida inizieranno la fase di iscrizione, accettazione e formazione gruppi.
**14:00:** *Dove: Area Hackathon* Termine delle iscrizioni

**14:00** *Dove: Area Hackathon* verrà effettuata una breve presentazione della sfida proposta da parte degli Owner
dell'evento. Subito a seguire i gruppi inizieranno a condividere e sviluppare le idee.


## Domenica 08 Settembre 2019
**14:00:** *Dove: Area Hackathon* Termine della sfida. Gli owner inviteranno i partecipanti a prendersi una breve pausa di relax

**14:30:-15:30** *Dove: Area Hackathon* I gruppi presenteranno le loro proposte

**15:30:-16:30** *Dove: Area Hackathon* La giuria si riunisce per valutare i gruppi e selezionare i vincitori, tenendo conto delle
esposizioni effettuate e del materiale prodotto, disponibile su https://gitlab.com/download-event-2019.

Alcuni criteri di valutazione suggeriti sono:

- Utilità / valore
- Attinenza alle Challenge proposte
- Completezza del progetto/prototipo presentato
- Design/User Experience
- Creatività/Innovazione

Al termine della discussione la Giuria avrà concordato:

- il gruppo con la migliore idea originale
- il gruppo con la migliore implementazione

I premi potranno essere assegnati a due progetti distinti oppure entrambi a un singolo gruppo

**16:30** *Dove: Area Hackaton* Un membro scelto della Giuria comunica a un rappresentante deglio Organizzatori dell'evento, nella figura di Luca Pedrazzini,
il nome del gruppo (o dei gruppi) vincitore/i.

**N.B.:** a causa del numero variabile dei gruppi si consiglia di tenere un tempo massimo (es: 7 minuti)
per la presentazione di ogni gruppo. All'occorrenza in presenza di molti gruppi è possibile accorciare
la fase di discussione della giuria posticipandone l'inizio; la fase dovrà comunque terminare alle 16:30.

**~16:30** *Dove: Palco principale* Organizzatori, sponsor e autorità annunceranno il nome del gruppo (o gruppi) vincitore/i

# Griglia di valutazione

Di seguito una proposta di griglia di valutazione; la Giuria può utilizzarla ad
esempio concordando i punti assegnati da 1 a 5 per ciascun gruppo, oppure procedere
in maniera discorsiva tenendo conto dei parametri suggeriti per la valutazione e
definendone di nuovi.

```
Legenda

A: Utilità / valore
B: Attinenza alle Challenge proposte
C: Completezza del progetto/prototipo presentato
D: Design/User Experience
E: Creatività/Innovazione
T: Totale punteggio
P1: Vincitore per la migliore idea originale
P2: Vincitore per la migliore idea originale

NOME GRUPPO                   A     B     C     D     E     TOT     P1     P2

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

________________________________________________________________________________

```



