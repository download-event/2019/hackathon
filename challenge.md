# Download 2019 - Hackathon Challenge

## Introduzione

Questo documento, destinato ai partecipanti alla hackathon organizzata durante l'evento [Download Innovation & IT Festival](https://download-event.io), edizione 2019, contiene presentazione e informazioni relative alla sfida proposte.

## Indicazioni generali

In linea con lo spirito dell'evento:

> 2 giorni di studio, condivisione, progetti, ispirazioni, scoperte e creatività... un invito aperto a chi vuole conoscere, contribuire e confrontarsi sugli ultimi trend IT e sul tema Innovazione con mente aperta, curiosità ed entusiasmo.

per questa seconda edizione abbiamo pensato di rendere la sfida ancora più generica presentando un tema: le migliori idee arrivano dai Voi che avete accettato di mettervi alla prova!

Come nella scorsa edizione, la risposta alla sfida può essere presentata in diversi modi: idee, architetture, algoritmi, mockup, prototipi e tutto quanto possa stimolare la fantasia e favorire una partecipazione appassionata degli hacker di tutto il mondo.

Non ci sono restrizioni tecnologiche: usate basi di dati, strumenti, linguaggi, framework e piattaforme che più vi piacciono!.
Ricordate che il risultato dei vostri sforzi dovrà essere rilasciato con una licenza [Open Source](https://opensource.org/licenses).

----

## Il contesto

Il challenge per ruota intorno al concetto di "consapevolezza territoriale":

In un contesto cittadino esiste tipicamente un grande e variegato insieme
di informazioni legate sia geograficamente che temporalmente al territorio:

- Eventi culturali: mostre, fiere, etc.
- Iniziative legate alla politica: convegni, comizi, etc.
- Eventi legati ad associazioni dei cittadini
- Disponibilità di strutture pubbliche: piscine, campi sportivi, etc.
- Eventuali guasti o manutenzioni programmate: deviazioni, chiusura temporanea di fermata o linea, etc.

Ma non solo, ecco alcuni esempi:

- _"Mi interessano le offerte speciali dei supermercati vicino casa (Sì, esistono già applicazioni dedicate)"_
- _"C'è qualche torneo di bocce in Lombardia ad Agosto? Voglio sfruttare le ferie al meglio!"_
- _"Ho l'irrefrenabile istinto a iscrivermi ad un'Hackathon. Quali sono le più interessanti in Europa?"_

## La sfida

Come anticipato vogliamo lasciare libero spazio alla creatività dei partecipanti, quindi la sfida
è genericamente legata a idee per aggregazione e presentazione di dati:

- Di diversa natura
- Da fonti disparate (ufficiali, social, GIS, etc.)
- Legati al territorio
- Legati al tempo

## Note aggiuntive

### La valutazione

Il progetto presentato da ciascun team sarà valutato secondo i criteri di:

- Utilità / Valore per il committente
- Attinenza al tema e alle sfide presentate
- Completezza: verranno premiati i team che hanno saputo portare il progetto in fase più avanzata di elaborazione / realizzazione.
- Design / UX, interfaccia utente, facilità d'uso, appeal
- Creatività / Innovazione soprattutto nell'affrontare le challenge

In particolare verranno premiati i progetti (o il progetto) con:

- La migliore idea originale
- La migliore implementazione

## Pubblicazione progetti

Verranno valutati i progetti pubblicati entro i termini previsti sul [progetto gitlab ufficiale](https://gitlab.com/download-event-2019/) dell'evento. Durante lo svolgimento dell'hackathon, gli organizzatori procederanno alla creazione di un repository dedicato per ogni team e a garantire gli accessi ai membri (è richiesta la creazione di un account su gitlab.com).
E' consentito utilizzare altre piattaforme per la condivisione del codice e in alternativa:

- Effettuare un push finale sul repository del proprio team.
- Impostare mirror di un repository (es: progetto su github.com viene riportato su gitlab.com)

----

Buon divertimento,  
*SR2*