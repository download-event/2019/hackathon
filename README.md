# Download 2019 Hackathon

Questo repository contiene informazioni e documentazione relativa all'Hackathon organizzata durante l'evento [Download Innovation & IT Festival](https://download-event.io), edizione 2019, svoltosi a Bergamo nelle giornate di Sabato 7 e Domenica 8 Settembre 2019.

# Collegamenti

- [Sito ufficiale dell'evento](https://download-event.io)
- [Regolamento generale hackathon](https://download-event.io/wp-content/uploads/2019/06/regolamento-generale-hackathon-2019.pdf)

# Documenti

- [Descrizione del challenge](challenge.md)
- [Informazioni per la giuria](jury.md)

Durante l'evento è possibile partecipare a discussione pubblica su Gitter
nella stanza https://gitter.im/download-event/hackathon

# Team partecipanti

I repository delle proposte realizzate sono presenti nel gruppo gitlab [download-event-2019](https://gitlab.com/download-event-2019)

## Klar

Paolo Campanelli, Alessandro Locatelli

[Proposta presentata](https://gitlab.com/download-event-2019/team-klar)

## EsperiaDev

Cristian Livella, Cristian Tironi, Marco Biasion, Filippo Colpani, Icore Gianmaria Falcone

[Proposta presentata](https://gitlab.com/download-event-2019/team-esperiadev)

## // TODO: team name

Davide Riva, Laura Nesossi, Federico Lodovici

[Proposta presentata](https://gitlab.com/annierjose87/team-todo-team-name)

## 100 e lode

Simone Ronzoni, Xhorxho Papallazzi, Pablo Campana

[Proposta presentata](https://gitlab.com/download-event-2019/team-100-e-lode)

## 404: TeamNotFound

Emanuele Sacco, Valerio Bonacina, Edoardo Fava, Pietro Cappellini, Ettore Murfuni

[Proposta presentata](https://gitlab.com/download-event-2019/team-404-team-not-found)

##  Minified

Alessandro Maglioccola, Andrea Chirico, Emilio Annoni, Alberto Redolfi

[Proposta presentata](https://gitlab.com/download-event-2019/team-minified)

